FROM golang:1.15.7-alpine as build

WORKDIR /cheyenne
COPY . .
RUN go build -o . ./...

FROM alpine:3.13
COPY --from=build /cheyenne/cheyenne /
ENTRYPOINT ["/cheyenne"]
