package docs

import (
	"gitlab.com/j0shgrant/cheyenne/internal/routing/registration"
	"go/types"
)

// swagger:route POST /register/user Registration registerUser
// Registers a new user with a given username, returning their UUID.
// responses:
//   201: createdResponse
//   400: badRequestResponse

// An empty response body is returned when a password is successfully registered.
// swagger:response createdResponse
type registerUserCreatedResponseWrapper struct {
	// in:body
	Body registration.CreatedUserResponse
}

// An error message is returned when a password request is incorrectly structured
// swagger:response badRequestResponse
type registerUserBadRequestResponseWrapper struct{
	// in:body
	Body types.Nil
}

// swagger:parameters registerUser
type registerUserParamsWrapper struct {
	// A JSON pair of the given UUID, and password to be registered.
	// in:body
	Body registration.CreateUserRequest
}
