// Documentation of Cheyenne API.
//
//		Schemes: http
//		BasePath: /
//		Version: 1.0.0
//
//		Consume:
//		- application/json
//
//		Produces:
//		- application/json
//
// swagger:meta
package docs
