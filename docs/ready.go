package docs

import "gitlab.com/j0shgrant/cheyenne/internal/routing/probing"

// swagger:route GET /ready Health readiness
// Readiness check returning the availability of Cheyenne and its dependencies.
// responses:
//   200: readinessResponse
//   503: readinessResponse

// Availability of Cheyenne's dependencies.
// swagger:response readinessResponse
type readinessResponseWrapper struct {
	// in:body
	Body probing.ReadyCheckResponse
}
