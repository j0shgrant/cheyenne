package docs

import "go/types"

// swagger:route GET /live Health liveness
// Liveness check which returns if Cheyenne is running.
// responses:
//   200: liveResponse

// An empty response body is returned if Cheyenne is live.
// swagger:response liveResponse
type liveResponseWrapper struct {
	// in:body
	Body types.Nil
}
