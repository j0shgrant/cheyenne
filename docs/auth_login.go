package docs

import (
	"gitlab.com/j0shgrant/cheyenne/internal/routing/auth"
	"go/types"
)

// swagger:route POST /auth/login Authentication login
// Authenticates against Cheyenne with a username/password pair, returning an authentication token with UUID.
// responses:
//   201: loginAuthenticatedResponse
//   400: badRequestResponse
//   401: loginUnauthorisedResponse

// Upon successful authentication, a token with one hour expiry is returned.
// swagger:response loginAuthenticatedResponse
type loginAuthenticatedResponseWrapper struct {
	// in:body
	Body auth.LoginResponse
}

// An error message is returned when a password request is incorrectly structured
// swagger:response badRequestResponse
type loginBadRequestResponseWrapper struct{
	// in:body
	Body types.Nil
}

// Upon failed authentication, an empty response body is returned.
// swagger:response loginUnauthorisedResponse
type loginUnauthorisedResponseWrapper struct{
	// in:body
	Body types.Nil
}

// swagger:parameters login
type loginParamsWrapper struct {
	// A JSON pair of username and password to authenticate with.
	// in:body
	Body auth.LoginRequest
}
