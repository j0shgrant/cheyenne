package docs

import "go/types"

// swagger:route POST /auth/logout Authentication logout
// Revokes a given authentication token for a given UUID.
// responses:
//   200: logoutSuccessfulResponse
//   401: logoutUnauthorisedResponse

// Upon successful token revocation, an empty response body is returned.
// swagger:response logoutSuccessfulResponse
type logoutSuccessfulWrapper struct {
	// in:body
	Body types.Nil
}

// Upon failed authentication, an empty response body is returned.
// swagger:response logoutUnauthorisedResponse
type logoutUnauthorisedResponseWrapper struct{
	// in:body
	Body types.Nil
}

// swagger:parameters logout
type logoutParamsWrapper struct {
	// Authentication token provided by the login endpoint.
	// in:header
	// name: X-Session-Token
	XSessionToken string `json:"X-Session-Token"`

	// UUID token provided by the login endpoint.
	// in:header
	// name: X-User-Id
	XUserId string `json:"X-User-Id"`
}