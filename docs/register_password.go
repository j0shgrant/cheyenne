package docs

import (
	"gitlab.com/j0shgrant/cheyenne/internal/routing/registration"
	"go/types"
)

// swagger:route POST /register/password Registration registerPassword
// Registers a new password for a given UUID.
// responses:
//   201: passwordCreatedResponse
//   400: badRequestResponse

// An empty response body is returned when a password is successfully registered.
// swagger:response passwordCreatedResponse
type registerPasswordCreatedResponseWrapper struct {
	// in:body
	Body types.Nil
}

// An error message is returned when a password request is incorrectly structured
// swagger:response badRequestResponse
type registerPasswordBadRequestResponseWrapper struct {
	// in:body
	Body types.Nil
}

// swagger:parameters registerPassword
type registerPasswordParamsWrapper struct {
	// A JSON pair of the given UUID, and password to be registered.
	// in:body
	Body registration.CreatePasswordRequest
}