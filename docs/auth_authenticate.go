package docs

import "go/types"

// swagger:route POST /auth/authenticate Authentication authenticate
// Authenticates a given UUID against a given authentication token.
// responses:
//   200: authenticatedResponse
//   401: unauthenticatedResponse

// Upon successful authentication, an empty response body is returned.
// swagger:response authenticatedResponse
type authenticatedResponseWrapper struct {
	// in:body
	Body types.Nil
}

// Upon failed authentication, an empty response body is returned.
// swagger:response unauthenticatedResponse
type unauthenticatedResponseWrapper struct{
	// in:body
	Body types.Nil
}

// swagger:parameters authenticate
type authenticationParamsWrapper struct {
	// Authentication token provided by the login endpoint.
	// in:header
	// name: X-Session-Token
	XSessionToken string `json:"X-Session-Token"`

	// UUID token provided by the login endpoint.
	// in:header
	// name: X-User-Id
	XUserId string `json:"X-User-Id"`
}