# Cheyenne
Cheyenne is a simple, lightweight, and completely open source authentication service written in Go.

## Getting started
To run Cheyenne locally, you will need to have a supporting instance of both MySQL and Redis. In order to make this startup easier, Cheyenne comes bundled with an quickstart `docker-compose.yml` to spin up the latest version of the service, along with its dependencies, using [Docker Compose](https://docs.docker.com/compose/install/). Spinning up the stack with default configuration is as simple as running the below command:

```$ docker-compose up```

## Running Cheyenne
When running Cheyenne in production, it is recommended to run it against a pre-existing or dedicated instance of both Redis and MySQL. In order to be able to connect to your existing or dedicated supporting services, Cheyenne is capable of being configured using the below environment variables:

| Environment Variable    | Default Value | Type   | Description                                                    |
| ----------------------- | ------------- | ------ | -------------------------------------------------------------- |
| CHEYENNE_HTTP_HOSTNAME  | localhost     | string | The hostname that Cheyenne will serve HTTP traffic on.         |
| CHEYENNE_HTTP_PORT      | 8080          | int    | The port that Cheyenne will server HTTP traffic on.            |
| CHEYENNE_MYSQL_USER     | cheyenne      | string | The username that Cheyenne will use to authenticate with MySQL |
| CHEYENNE_MYSQL_PASSWORD | password      | string | The password that Cheyenne will use to authenticate with MySQL |
| CHEYENNE_MYSQL_HOSTNAME | localhost     | string | The hostname that Cheyenne will use to connect with MySQL      |
| CHEYENNE_MYSQL_PORT     | 3306          | int    | The port that Cheyenne will use to connect with MySQL          |
| CHEYENNE_MYSQL_DB       | cheyenne      | string | The database the Cheyenne will use to persist data in MySQL    |
| CHEYENNE_REDIS_HOSTNAME | localhost     | string | The hostname that Cheyenne will use to connect with Redis      |
| CHEYENNE_REDIS_PORT     | 6379          | int    | The port that Cheyenne will use to connect with Redis          |
| CHEYENNE_REDIS_PASSWORD | <empty>       | string | The password that Cheyenne will use to authenticate with Redis |
| CHEYENNE_REDIS_DB       | 0             | int    | The database that Cheyenne will use to persist data in Redis   |

## API Reference
API documentation for Cheyenne can be found [here](https://j0shgrant.gitlab.io/cheyenne).
