module gitlab.com/j0shgrant/cheyenne

go 1.15

require (
	github.com/go-redis/redis/v8 v8.4.10
	github.com/go-sql-driver/mysql v1.5.0
	github.com/google/uuid v1.2.0
	github.com/gorilla/mux v1.8.0
	github.com/jpillora/backoff v1.0.0
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
)
