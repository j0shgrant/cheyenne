package main

import (
	"context"
	"fmt"
	_ "gitlab.com/j0shgrant/cheyenne/docs"
	redis "gitlab.com/j0shgrant/cheyenne/internal/redis/client"
	"gitlab.com/j0shgrant/cheyenne/internal/routing"
	"gitlab.com/j0shgrant/cheyenne/internal/service/auth"
	"gitlab.com/j0shgrant/cheyenne/internal/service/password"
	"gitlab.com/j0shgrant/cheyenne/internal/service/user"
	"gitlab.com/j0shgrant/cheyenne/internal/sql/cheyenne"
	sql "gitlab.com/j0shgrant/cheyenne/internal/sql/client"
	"gitlab.com/j0shgrant/cheyenne/internal/util"
	"go.uber.org/zap"
	"net/http"
	"time"
)

func main() {

	// Configure Zap for logging
	logger, err := zap.NewProduction()
	if err != nil {
		zap.S().Fatal(err)
	}
	zap.ReplaceGlobals(logger)
	defer logger.Sync()

	// Initialise DB connectivity and create password table
	db, err := sql.NewClient(sql.LoadParams())
	if err != nil {
		zap.S().Fatal(err)
	}

	_, err = cheyenne.CreatePasswordTable(db)
	if err != nil {
		zap.S().Fatal(err)
	}
	_, err = cheyenne.CreateUserTable(db)
	if err != nil {
		zap.S().Fatal(err)
	}

	// Initialise Redis connectivity
	ctx := context.Background()
	cache, err := redis.NewCache(redis.LoadParams(), ctx)
	if err != nil {
		zap.S().Fatal(err)
	}
	defer cache.Close()

	// Create PasswordEngine
	passwordEngine := &password.Engine{
		DB: db,
	}

	// Create UserEngine
	userEngine := &user.Engine{
		DB: db,
	}

	// Create AuthEngine
	authEngine := &auth.Engine{
		PasswordEngine: passwordEngine,
		Cache:          cache,
		Context:        ctx,
	}


	// Configure HTTP server
	httpHostname := util.GetEnvString("CHEYENNE_HTTP_HOSTNAME", "localhost")
	httpPort := util.GetEnvInt("CHEYENNE_HTTP_PORT", 8080)

	server := &http.Server{
		Handler:      routing.NewRouter(authEngine, passwordEngine, userEngine),
		Addr:         fmt.Sprintf("%s:%d", httpHostname, httpPort),
		WriteTimeout: 10 * time.Second,
		ReadTimeout:  10 * time.Second,
	}

	// Serve traffic
	zap.S().Infof("Listening at [%s].", fmt.Sprintf("http://%s:%d", httpHostname, httpPort))
	zap.S().Fatal(server.ListenAndServe())
}
