package util

import (
	"go.uber.org/zap"
	"os"
	"strconv"
)

func GetEnvString(key string, defaultValue string) string {
	result, exists := os.LookupEnv(key)
	if exists {
		return result
	} else {
		return defaultValue
	}
}

func GetEnvInt(key string, defaultValue int) int {
	result, exists := os.LookupEnv(key)
	if exists {
		parsed, err := strconv.Atoi(result)
		if err == nil {
			return parsed
		} else {
			zap.S().Error("Failed to read environment variable: [%s] - terminating.", key)
			zap.S().Error(err)
			panic(err)
		}
	} else {
		return defaultValue
	}
}
