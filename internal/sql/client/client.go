package client

import (
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jpillora/backoff"
	"gitlab.com/j0shgrant/cheyenne/internal/util"
	"go.uber.org/zap"
	"time"
)

type ConnectionParams struct {
	User     string
	Password string
	Database string
	Hostname string
	Port     int
}

func LoadParams() *ConnectionParams {
	return &ConnectionParams{
		User:     util.GetEnvString("CHEYENNE_MYSQL_USER", "cheyenne"),
		Password: util.GetEnvString("CHEYENNE_MYSQL_PASSWORD", "password"),
		Database: util.GetEnvString("CHEYENNE_MYSQL_DB", "cheyenne"),
		Hostname: util.GetEnvString("CHEYENNE_MYSQL_HOSTNAME", "localhost"),
		Port:     util.GetEnvInt("CHEYENNE_MYSQL_PORT", 3306),
	}
}

func NewClient(params *ConnectionParams) (db *sql.DB, err error) {
	backoffTimer := 60
	db, err = retryConnection(buildConnectionString(params), backoffTimer)
	if err != nil {
		return nil, err
	}

	db.SetConnMaxLifetime(time.Minute * 5)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	return db, nil
}

func buildConnectionString(p *ConnectionParams) string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s",
		p.User,
		p.Password,
		p.Hostname,
		p.Port,
		p.Database,
	)
}

func retryConnection(connectionString string, backoffTimer int) (*sql.DB, error) {
	max := time.Duration(backoffTimer) * time.Second
	b := &backoff.Backoff{
		Min: 2000 * time.Millisecond,
		Max: max,
	}
	for {
		d := b.Duration()
		if d < max {
			db, err := sql.Open("mysql", connectionString)
			if err == nil {
				err = db.Ping()
				if err != nil {
					err = nil
					zap.S().Infof("Failed to connect to MySQL, retrying in %dms", d / 1000000)
					time.Sleep(d)
				} else {
					zap.S().Info("Successfully connected to MySQL.")
					return db, nil
				}
			}
		} else {
			return nil, errors.New(fmt.Sprintf("Timed out attempting to connect to MySQL after %d seconds.", backoffTimer))
		}
	}
}
