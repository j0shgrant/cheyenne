package cheyenne

import (
	"database/sql"
	"errors"
	"fmt"
	uuid2 "github.com/google/uuid"
	"go.uber.org/zap"
)

func CreatePasswordTable(db *sql.DB) (affectedRows int64, err error) {
	query := `CREATE TABLE IF NOT EXISTS password (
	uuid VARBINARY(255) NOT NULL PRIMARY KEY,
	salt VARBINARY(255) NOT NULL,
	hash VARBINARY(255) NOT NULL
);`

	statement, err := db.Prepare(query)
	if err != nil {
		return 0, err
	}
	defer statement.Close()

	result, err := statement.Exec()
	if err != nil {
		return 0, err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}

	return rowsAffected, nil
}

func CreateUserTable(db *sql.DB) (affectedRows int64, err error) {
	query := `CREATE TABLE IF NOT EXISTS user (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	username VARCHAR(255) NOT NULL,
	uuid VARCHAR(255) NOT NULL
);`

	statement, err := db.Prepare(query)
	if err != nil {
		return 0, err
	}
	defer statement.Close()

	result, err := statement.Exec()
	if err != nil {
		return 0, err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}

	return rowsAffected, nil
}

func CreatePassword(db *sql.DB, uuid string, salt []byte, hash []byte) (affectedRows int64, err error) {
	query := fmt.Sprintf("INSERT INTO password (uuid, salt, hash) VALUES ('%s', '%s', '%s');", uuid, salt, hash)

	statement, err := db.Prepare(query)
	if err != nil {
		return 0, err
	}
	defer statement.Close()

	result, err := statement.Exec()
	if err != nil {
		return 0, err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	if rowsAffected != 1 {
		return 0, errors.New("failed to create password record")
	}

	return rowsAffected, nil
}

func CreateUser(db *sql.DB, username string) (uuid string, err error) {
	uuid = uuid2.New().String()
	query := fmt.Sprintf("INSERT INTO user (username, uuid) VALUES ('%s', '%s');", username, uuid)

	statement, err := db.Prepare(query)
	if err != nil {
		return "", err
	}
	defer statement.Close()

	result, err := statement.Exec()
	if err != nil {
		return "", err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return "", err
	}
	if rowsAffected != 1 {
		return "", errors.New("failed to create user record")
	}

	return uuid, nil
}

func GetPassword(db *sql.DB, uuid string) (salt []byte, hash []byte, err error) {
	query := fmt.Sprintf("SELECT salt, hash FROM password WHERE uuid = '%s';", uuid)

	statement, err := db.Prepare(query)
	if err != nil {
		return nil, nil, err
	}
	defer statement.Close()

	rows, err := statement.Query()
	if err != nil {
		return nil, nil, err
	}

	// Extract salt+hash && handle duplicate records
	salt = make([]byte, 64)
	hash = make([]byte,64)
	for rows.Next() {
		err = rows.Scan(&salt, &hash)
		if err != nil {
			return nil, nil, err
		}

		if rows.Next() {
			errorMsg := fmt.Sprintf("Multiple password records found for uuid [%s].", uuid)
			zap.S().Error(errorMsg)
			return nil, nil, errors.New(errorMsg)
		}
	}

	return salt, hash, nil
}

func GetUser(db *sql.DB, username string) (uuid string, err error) {
	query := fmt.Sprintf("SELECT uuid FROM user WHERE username = '%s';", username)

	statement, err := db.Prepare(query)
	if err != nil {
		return "", err
	}
	defer statement.Close()

	rows, err := statement.Query()
	if err != nil {
		return "", err
	}

	uuidBytes := make([]byte, 256)
	for rows.Next() {
		err = rows.Scan(&uuidBytes)
		if err != nil {
			return "", err
		}

		if rows.Next() {
			errorMsg := fmt.Sprintf("Multiple user records found for username [%s].", username)
			zap.S().Error(errorMsg)
			return "", errors.New(errorMsg)
		}
	}

	return string(uuidBytes), nil
}
