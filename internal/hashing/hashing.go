package hashing

import (
	"crypto/rand"
	"golang.org/x/crypto/argon2"
)

func GenerateHashAndSalt(password string) (hash []byte, salt []byte, err error) {
	salt, err = generateSalt(16)
	if err != nil {
		return nil, nil, err
	}

	hash = argon2IdHash(pepperPassword([]byte(password)), salt)

	return hash, salt, nil
}

func RebuildHash(password string, salt []byte) []byte {
	return argon2IdHash(pepperPassword([]byte(password)), salt)
}

func argon2IdHash(input []byte, salt []byte) []byte {
	return argon2.IDKey(input, salt, 3, 64*1024, 1, 32)
}

func generateSalt(n uint32) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)

	if err != nil {
		return nil, err
	}

	return b, nil
}

func pepperPassword(password []byte) []byte {
	pepper := []byte("40gatGt4XEQEIdH6")
	return append(pepper, password...)
}
