package cheyenne

import (
	"context"
	"github.com/go-redis/redis/v8"
	token "github.com/google/uuid"
	"time"
)

func CreateToken(cache *redis.Client, ctx context.Context, uuid string) (apiToken string, err error) {
	apiToken = token.New().String()
	err = cache.Set(ctx, uuid, apiToken, time.Hour).Err()
	if err != nil {
		return "", err
	}

	return apiToken, nil
}

func GetToken(cache *redis.Client, ctx context.Context, uuid string) (apiToken string, exists bool, err error) {
	apiToken, err = cache.Get(ctx, uuid).Result()
	if err == redis.Nil {
		return apiToken, false, nil
	}
	if err != nil {
		return apiToken, false, err
	}

	return apiToken, true, nil
}

func DeleteToken(cache *redis.Client, ctx context.Context, uuid string) error {
	err := cache.Del(ctx, uuid).Err()
	if err != nil {
		return err
	}

	return nil
}