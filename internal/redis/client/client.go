package client

import (
	"context"
	"errors"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/jpillora/backoff"
	"gitlab.com/j0shgrant/cheyenne/internal/util"
	"go.uber.org/zap"
	"time"
)

type ConnectionParams struct {
	Hostname string
	Port     int
	DB       int
	Password string
}

func LoadParams() *ConnectionParams {
	return &ConnectionParams{
		Hostname: util.GetEnvString("CHEYENNE_REDIS_HOSTNAME", "localhost"),
		Port:     util.GetEnvInt("CHEYENNE_REDIS_PORT", 6379),
		DB:       util.GetEnvInt("CHEYENNE_REDIS_DB", 0),
		Password: util.GetEnvString("CHEYENNE_REDIS_PASSWORD", ""),
	}
}

func NewCache(p *ConnectionParams, ctx context.Context) (client *redis.Client, err error) {
	backoffTimer := 32
	redisAddr := fmt.Sprintf("%s:%d", p.Hostname, p.Port)
	redisOptions := &redis.Options{
		Addr:     redisAddr,
		Password: p.Password,
		DB:       p.DB,
	}

	client, err = retryConnection(redisOptions, ctx, backoffTimer)
	if err != nil {
		return nil, err
	}

	return client, nil
}

func retryConnection(options *redis.Options, ctx context.Context, backoffTimer int) (*redis.Client, error) {
	max := time.Duration(backoffTimer) * time.Second
	b := &backoff.Backoff{
		Min: 2000 * time.Millisecond,
		Max: max,
	}
	client := redis.NewClient(options)
	for {
		d := b.Duration()
		if d < max {
			if client != nil {
				err := client.Ping(ctx).Err()
				if err != nil {
					err = nil
					zap.S().Infof("Failed to connect to Redis, retrying in %dms", d/1000000)
					time.Sleep(d)
				} else {
					zap.S().Info("Successfully connected to Redis.")
					return client, nil
				}
			} else {
				zap.S().Info("Failed to create Redis client.")
				zap.S().Infof("Failed to connect to Redis, retrying in %dms", d/1000000)
				time.Sleep(d)
			}
		} else {
			return nil, errors.New(fmt.Sprintf("Timed out attempting to connect to Redis after %d seconds.", backoffTimer))
		}
	}
}
