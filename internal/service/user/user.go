package user

import (
	"database/sql"
	"gitlab.com/j0shgrant/cheyenne/internal/sql/cheyenne"
)

type Engine struct {
	DB *sql.DB
}

func (userEngine *Engine) Create(username string) (uuid string, err error) {
	uuid, err = cheyenne.CreateUser(userEngine.DB, username)
	if err != nil {
		return "", err
	}

	return uuid, nil
}

func (userEngine *Engine) GetUuid(username string) (uuid string, err error) {
	uuid, err = cheyenne.GetUser(userEngine.DB, username)
	if err != nil {
		return "", err
	}

	return uuid, nil
}