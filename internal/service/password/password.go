package password

import (
	"crypto/subtle"
	"database/sql"
	"gitlab.com/j0shgrant/cheyenne/internal/hashing"
	"gitlab.com/j0shgrant/cheyenne/internal/sql/cheyenne"
)

type Engine struct {
	DB *sql.DB
}

func (passwordEngine *Engine) Create(uuid string, password string) error {
	hash, salt, err := hashing.GenerateHashAndSalt(password)
	if err != nil {
		return err
	}

	_, err = cheyenne.CreatePassword(passwordEngine.DB, uuid, salt, hash)
	if err != nil {
		return err
	}

	return nil
}

func (passwordEngine *Engine) Validate(uuid string, password string) (isMatch bool, err error) {
	salt, actualHash, err := cheyenne.GetPassword(passwordEngine.DB, uuid)
	if err != nil {
		return false, err
	}

	expectedHash := hashing.RebuildHash(password, salt)
	isMatch = subtle.ConstantTimeCompare(actualHash, expectedHash) == 1

	return isMatch, nil
}
