package auth

import (
	"context"
	"github.com/go-redis/redis/v8"
	"gitlab.com/j0shgrant/cheyenne/internal/redis/cheyenne"
	"gitlab.com/j0shgrant/cheyenne/internal/service/password"
)

type Engine struct {
	Cache          *redis.Client
	PasswordEngine *password.Engine
	Context        context.Context
}

func (authEngine *Engine) Login(uuid string, password string) (token string, unauthorised bool, err error) {
	isMatch, err := authEngine.PasswordEngine.Validate(uuid, password)
	if err != nil {
		return "", true, err
	}
	if !isMatch {
		return "", true, nil
	}

	token, exists, err := cheyenne.GetToken(authEngine.Cache, authEngine.Context, uuid)
	if err != nil {
		return "", true, err
	}
	if exists {
		return token, false, nil
	}

	token, err = cheyenne.CreateToken(authEngine.Cache, authEngine.Context, uuid)
	if err != nil {
		return "", false, err
	}

	return token, false, nil
}

func (authEngine *Engine) Authenticate(uuid string, token string) (authenticated bool, err error) {
	expected, exists, err := cheyenne.GetToken(authEngine.Cache, authEngine.Context, uuid)
	if err != nil {
		return false, err
	}
	if !exists {
		return false, nil
	}

	authenticated = token == expected

	return authenticated, nil
}

func (authEngine *Engine) Deauthenticate(uuid string) error {
	_, exists, err := cheyenne.GetToken(authEngine.Cache, authEngine.Context, uuid)
	if err != nil {
		return err
	}

	if exists {
		err = cheyenne.DeleteToken(authEngine.Cache, authEngine.Context, uuid)
		if err != nil {
			return err
		}

		return nil
	}

	return nil
}
