package routing

import (
	"github.com/gorilla/mux"
	auth2 "gitlab.com/j0shgrant/cheyenne/internal/routing/auth"
	"gitlab.com/j0shgrant/cheyenne/internal/routing/probing"
	"gitlab.com/j0shgrant/cheyenne/internal/routing/registration"
	"gitlab.com/j0shgrant/cheyenne/internal/service/auth"
	"gitlab.com/j0shgrant/cheyenne/internal/service/password"
	"gitlab.com/j0shgrant/cheyenne/internal/service/user"
	"net/http"
)

func NewRouter(authEngine *auth.Engine, passwordEngine *password.Engine, userEngine *user.Engine) *mux.Router {
	// Create new mux Router
	r := mux.NewRouter()

	// Configure routing
	r.HandleFunc("/live", probing.LivenessHandler).Methods(http.MethodGet)
	r.HandleFunc("/ready", probing.ReadinessHandler(passwordEngine.DB, authEngine.Cache, authEngine.Context)).Methods(http.MethodGet)
	r.HandleFunc("/register/user", registration.UserCreationHandler(userEngine)).Methods(http.MethodPost)
	r.HandleFunc("/register/password", registration.PasswordCreationHandler(passwordEngine)).Methods(http.MethodPost)
	r.HandleFunc("/auth/login", auth2.LoginHandler(authEngine, userEngine)).Methods(http.MethodPost)
	r.HandleFunc("/auth/logout", auth2.LogoutHandler(authEngine)).Methods(http.MethodPost)
	r.HandleFunc("/auth/authenticate", auth2.AuthenticationHandler(authEngine)).Methods(http.MethodPost)

	return r
}
