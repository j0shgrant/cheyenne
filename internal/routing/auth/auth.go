package auth

import (
	"encoding/json"
	"gitlab.com/j0shgrant/cheyenne/internal/routing/common"
	"gitlab.com/j0shgrant/cheyenne/internal/service/auth"
	"gitlab.com/j0shgrant/cheyenne/internal/service/user"
	"net/http"
)

type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}
type LoginResponse struct {
	Uuid  string `json:"uuid"`
	Token string `json:"token"`
}

func LoginHandler(authEngine *auth.Engine, userEngine *user.Engine) common.Handler {
	return func(w http.ResponseWriter, r *http.Request) {
		var body LoginRequest
		decoder := json.NewDecoder(r.Body)

		err := decoder.Decode(&body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write([]byte(err.Error()))
			return
		}

		uuid, err := userEngine.GetUuid(body.Username)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write([]byte(err.Error()))
			return
		}

		token, unauthorised, err := authEngine.Login(uuid, body.Password)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write([]byte(err.Error()))
			return
		}
		if unauthorised {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		responseBody, err := json.Marshal(&LoginResponse{
			Token: token,
			Uuid:  uuid,
		})
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		_, _ = w.Write(responseBody)
		w.WriteHeader(http.StatusCreated)
	}
}

func LogoutHandler(authEngine *auth.Engine) common.Handler {
	return func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("X-Session-Token")
		uuid := r.Header.Get("X-User-Id")
		authenticated, err := authEngine.Authenticate(uuid, token)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write([]byte(err.Error()))
			return
		}
		if !authenticated {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		err = authEngine.Deauthenticate(uuid)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write([]byte(err.Error()))
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}

func AuthenticationHandler(authEngine *auth.Engine) common.Handler {
	return func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("X-Session-Token")
		uuid := r.Header.Get("X-User-Id")
		authenticated, err := authEngine.Authenticate(uuid, token)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write([]byte(err.Error()))
			return
		}
		if !authenticated {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}
