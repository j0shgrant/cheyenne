package registration

import (
	"encoding/json"
	"gitlab.com/j0shgrant/cheyenne/internal/routing/common"
	"gitlab.com/j0shgrant/cheyenne/internal/service/password"
	"gitlab.com/j0shgrant/cheyenne/internal/service/user"
	"go.uber.org/zap"
	"net/http"
)

type CreatePasswordRequest struct {
	UUID     string `json:"uuid"`
	Password string `json:"password"`
}

type CreateUserRequest struct {
	Username string `json:"username"`
}
type CreatedUserResponse struct {
	Uuid string `json:"uuid"`
}

func PasswordCreationHandler(passwordEngine *password.Engine) common.Handler {
	return func(w http.ResponseWriter, r *http.Request) {
		var body CreatePasswordRequest
		decoder := json.NewDecoder(r.Body)

		err := decoder.Decode(&body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write([]byte(err.Error()))
			return
		}

		err = passwordEngine.Create(body.UUID, body.Password)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write([]byte(err.Error()))
			zap.S().Errorf("Error creating password record: [%s]", err.Error())
			return
		}

		w.WriteHeader(http.StatusCreated)
	}
}

func UserCreationHandler(userEngine *user.Engine) common.Handler {
	return func(w http.ResponseWriter, r *http.Request) {
		var body CreateUserRequest
		decoder := json.NewDecoder(r.Body)

		err := decoder.Decode(&body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write([]byte(err.Error()))
			return
		}

		uuid, err := userEngine.Create(body.Username)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write([]byte(err.Error()))
			zap.S().Errorf("Error creating user record: [%s]", err.Error())
			return
		}

		responseBody, err := json.Marshal(&CreatedUserResponse{
			Uuid: uuid,
		})
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		} else {
			w.WriteHeader(http.StatusCreated)
			_, _ = w.Write(responseBody)
		}
	}
}
