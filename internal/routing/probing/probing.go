package probing

import (
	"context"
	"database/sql"
	"encoding/json"
	"github.com/go-redis/redis/v8"
	"gitlab.com/j0shgrant/cheyenne/internal/routing/common"
	"net/http"
)

type ReadyCheckResponse struct {
	MySQL bool `json:"mysql"`
	Redis bool `json:"redis"`
}

func LivenessHandler(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func ReadinessHandler(db *sql.DB, cache *redis.Client, ctx context.Context) common.Handler {
	return func(w http.ResponseWriter, _ *http.Request) {
		redisReady := cache.Ping(ctx).Err()
		sqlReady := db.Ping()

		if redisReady != nil || sqlReady != nil {
			w.WriteHeader(http.StatusServiceUnavailable)
		} else {
			w.WriteHeader(http.StatusOK)
		}

		responseBody, err := json.Marshal(&ReadyCheckResponse{
			MySQL: sqlReady == nil,
			Redis: redisReady == nil,
		})
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

		_, _ = w.Write(responseBody)

	}
}